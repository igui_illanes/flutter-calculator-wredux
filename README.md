# Flutter Calculator wRedux

![App preview](https://media2.giphy.com/media/l0tunslsYhbr3zk5oG/giphy.gif?cid=790b761154d69c000b8dc409f394bc4ff22b5673944ec3c3&rid=giphy.gif)

This is a simple example of a calculator made with Flutter and [flutter_redux](https://pub.dev/packages/flutter_redux) for state management!

This example features:
 - Basic mathematical operations
 - Multiple operations made in a single line or entry
 - History to track all of the operations made by the calculator

# Dependencies

 - [flutter_redux: ^0.10.0](https://pub.dev/packages/flutter_redux)
 - [redux: ^5.0.0](https://pub.dev/packages/redux)
 - [sqflite: ^2.0.3+1](https://pub.dev/packages/sqflite)
 - [path_provider: ^2.0.11](https://pub.dev/packages/path_provider)

**Make sure to pub get before compiling!**

    flutter pub get
